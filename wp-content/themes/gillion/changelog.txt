Version 1.5.0
* Added - Wishlist feature for WooCommerce shop
* Added - WooCommerce categories element
* Added - WooCommerce spotlight element
* Added - WooCommerce products (dynamic) element
* Added - Icons for Gillion WooCommerce elements
* Added - Option to show black and white image for Shufflehound image widget
* Added - Option to disable heading vertical accent line
* Added - Option to change style for Shufflehound subscribe widget
* Improved - Multiple product related widget design
* Improved - Load more button design
* Fixed - 3 columns in basic posts not working when using sidebar
* Fixed - Blog posts offset issue
* Fixed - Outdated Woocommerce files issue

Version 1.4.3
* Added - premium plugin "Revolution Slider"
* Added - option to offset blog page template posts
* Added - option to change blog page template posts order
* Added - option to enable or disable WooCommerce cart icon in header
* Added - option to enable or disable all blog posts meta data
* Improved - Guest bookmark now opens login popup instead of showing tooltip
* Fixed - Posts tabs and recent posts widgets will ignore sticky posts
* Fixed - Text which can't be translated
* Fixed - Share button not visible in mobile devices
* Fixed - "Blog posts categories" element sometimes doesn't work

Version 1.4.2
* Added - Option to offset "Blog Posts" and "Blog Posts Basic" element posts
* Added - Option to show image captions in single pages
* Improved - Inline mobile navigation links now closes dropdown
* Improved - Post view counting feature by skipping unpublished post views and admin views
* Updated - "Shufflehound Instagram" widget is now outdated method and it's usage isn't recommended
* Fixed - Order option not working correctly in "Blog Posts" and "Blog Posts Basic" element posts
* Fixed - Blog page and mega menu options doesn't show all categories
* Fixed - Fatal error in while getting started with theme
* Fixed - Syntax issue regarding to multiple similar ID
* Fixed - Infinite post loading issue in some cases
* Fixed - Multiple responsiveness related issues
* Fixed - Pagination alignment not working

Version 1.4.1
* Added - New posts loading method "Load more" button (under edit page - page settings)
* Added - New posts loading method "Infinite" scrolling (under edit page - page settings)
* Added - Option to select/customize social share icons (12 in total)
* Improved - WooCommerce compatibility
* Fixed - "Blog Posts Categories" not working correctly in some cases
* Fixed - Some not translation ready words

Version 1.4.0
* Added - Basic WooCommerce support
* Fixed - "Blog Posts Categories" categories not showing up in some cases
* Fixed - "Blog Posts Categories" spacing issue
* Fixed - "Blog Posts" element responsivnesse issue with 3 columns
* Fixed - file:"helpers_blog.php" related error issue

Version 1.3.6
* Improved - Demo content installation page with more information about demo contents
* Updated - WPBakery Page Builder (formerly Visual Composer) to the latest version
* Fixed - Empty masonry post page in mobile devices

Version 1.3.5
* Fixed - Unyson Framework color picker is not working correctly in the latest version

Version 1.3.4
* Added - Gizmo News demo
* Added - Styling options for blog text slider
* Added - Envato Market Plugin for theme automatic updates
* Improved - Mega menu carousel buttons now hides, when not used
* Improved - Theme files structure
* Improved - Some blog post responsiveness

Version 1.3.3
* Added - Blog text Slider element
* Fixed - Missing Unyson color picker issue
* Notice - Please skip Unyson 2.7.9 version if possible

Version 1.3.2
* Added - Categories page layout option under Blog
* Updated - Visual Composer to the latest version
* Improved - Reduced theme size to more than 50%
* Improved - Theme installation time
* Fixed - Comments responsive issue
* Fixed - Blog slider (style 2,8), blog posts (card style) responsive issues
* Improved - Closed comments design

Version 1.3.1
* Added - Missing template images
* Updated - Slick.js carousel plugin

Version 1.3.0
* Added - Template system for more easier page building in Visual Composer
* Renamed - "Blog Posts element" to "Blog Posts Basic"
* Renamed - "Blog Posts Fancy" element to "Blog Posts"
* Improved - "Blog Posts Basic", "Blog Posts" and "Blog Posts Categories" element options for ease of use
* Improved - "Blog Categories" and "Blog Posts Categories" element which now shows most popular categories by default
* Improved - Visual Composer template and element window design
* Improved - Gillion element descriptions for better understanding
* Fixed - "Blog Posts" element cover style loading animation and padding
* Fixed - "Blog Posts" element limit option issue for 4 posts
* Fixed - "Blog Posts Categories" element style 1 thumbnail design
* Fixed - "Blog Slider" and "Blog Posts" elements some times freezes Visual Composer editor

Version 1.2.8
* Getting ready for new element template system
* Improved - Blog slider whitespaces
* Improved - Blog fancy posts option descriptions
* Fixed - Blog posts categories element review crop issue
* Fixed - Blog slider JavaScript issue in Visual Composer

Version 1.2.7
* Fixed mega menu date issue
* Fixed login popup not working in some cases
* Fixed blog slider style 4 autoplay not working

Version 1.2.6
* Updated - Visual Composer plugin
* Fixed - Header search icon doesn't trigger submit

Version 1.2.5
* Fixed - Slider responsiveness issues
* Fixed - Notice remember feature not working
* Fixed - Can't disable sticky header
* Fixed - Fatal error is some cases

Version 1.2.4
* Added - Travel demo
* Updated - TGMPA to latest version
* Updated - POT file
* Improved - Image crop in a few places
* Fixed - Can't disable sticky header

Version 1.2.3
* Added - Autoplay option for blog fancy posts
* Added - Heading V2 element font change option
* Added - Blog Posts Categories element Style 3
* Added - Blog style Left/Right Mix (small)
* Added - Blog slider uppercase title option
* Added - Blog slider option to include some of new posts
* Improved - Blog posts categories element style 2 design
* Fixed - Large style blank element when no thumbnail selected
* Fixed - Blog posts categories element whitespaces
* Fixed - Log in/log out buttons translation issue

Version 1.2.2
* Added - new improved demo content installation method
* Optimized - Image sizes for better loading speeds
* Fixed - Footer Instagram alignment issue
* Fixed - Categories element alignment issue
* Fixed - Comments large whitespaces

Version 1.2.1
* Added mega menu posts loading animation
* Added option to set topbar background image
* Added global page layout option and improved description
* Added option to show Twitter widget feed images
* Added login icon option for navigation
* Improved social networks element styling
* Improved sidebar responsiveness
* Fixed mobile search icon position
* Fixed Internet Explorer masonry issue
* Fixed post gallery random option not working
* Fixed blog slider categories not working
* Fixed header side menu widget loop issue
* Fixed documentation and support forum sections not working in some cases

Version 1.2.0
* Added autoplay option with speed control for blog slider element
* Added arrow controls for a few blog slider layouts
* Added/updated a few plugins including Visual Composer
* Added option to hide related posts
* Improved post content galleries design with a new options
* Improved Posts Slider widget ease of use and renamed to Recent Posts V2
* Improved mega menu design for light header
* Improved masonry layout loading performance
* Improved content editor visual layout
* Improved lightbox functionality
* Improved related posts functionality
* Fixed mega menu post slider animation issue
* Fixed blog slider issue with sticky posts
* Fixed header search box typo

Version 1.1.4
* Added hover colors for social icons dropdown
* Added mega menu categories in responsive navigation
* Improved overall page loading performance
* Improved side menu and mega menu design
* Improved page content styling
* Fixed left post style responsive issue
* Fixed a few SEO related issues

Version 1.1.3
* Added new element - Image container
* Added gradient background option for button element
* Added autoplay option for image gallery element

Version 1.1.2
* Added fashion demo
* Added new blog post style
* Added image gallery and social networks elements
* Added new color option for button element

Version 1.1.1
* Added personal and clean demos
* Added custom description for all Gillion Visual Composer elements
* Added option to change opened post content font size
* Fixed Visual Composer 5.2 frontend JavaScript issue
* Fixed prev/next post block render issue
* Improved single post slider responsivness

Version 1.1.0
* Added foodie demo
* Added new element - Button V2
* Added new page layouts
* Added new blog page styles
* Added new blog slider element style
* Added new blog fancy posts element styles
* Added new options for Text Block V2 and Heading V2 elements
* Added new option to change featured blog post style
* Added new option to set all blog post titles to uppercase
* Added new option to disable side menu icon in header
* Added new option to change widget heading font weight
* Added new option to change footer widget section padding
* Added new option for about us widgets
* Added shortcode support for Text Block V2 element
* Added "MailChimp for WordPress" to recommended plugin list
* Updated Visual Composer to the latest version
* Improved sticky header scrolling for layout 3
* Improved backend output for gillion custom elements
* Improved theme settings styling and ease of use
* Fixed design options not working for multiple shufflehound elements
* Fixed large image issue under featured post option
* Fixed blog posts element column typo
* Little code clean up

Version 1.0.11
* Added social share bar for mobile devices in single page
* Added option to change text weight for Text Block V2
* Added option to set text to uppercase for Text Block V2
* Added option to add shadow for Text Block V2
* Added option to set line height for Text Block V2
* Fixed padding/margin option for Text Block V2
* Changed Text block V2 default Text

Version 1.0.10
* Fixed fatal error when Visual Composer is disabled
* Fixed view count is still visible after disable

Version 1.0.9
* Added login/signup button/lightbox in header
* Added new tech demo
* Added new slider layout
* Added new posts layout
* Improved fancy blog posts element
* Fixed fatal error for widgets when Unyson plugin isn't activated

Version 1.0.8
* Added basic RTL support
* Improved theme translations
* Updated language .pot file
* Removed some debugging related information

Version 1.0.7
* Added Lifestyle demo
* Added Heading V2 and Text Block V2 elements to Visual Composer
* Added option to center footer copyrights information
* Added MailChimp for WordPress plugin support
* Added option to add Instagram footer title
* Updated customizer options
* Improved customizer ease of use
* Improved footer styling options
* Improved theme responsive layouts
* Updated Revolution Slider plugin
* Fixed header style 4 issues
* Fixed comments element link positon
* Fixed author page SEO issue
* Various other improvements

Version 1.0.6
* Various SEO improvements

Version 1.0.5
* Add 2 new slider layouts
* Added new element Separator with Text 2
* Added option to change categories font
* Added option to change slider categories color
* Added Mix style large for blog posts
* Improved SEO in single post page
* Improved theme translation and updated translation file
* Fixed issue when header bookmark icon can't be disabled
* Various other improvements

Version 1.0.4
* Added option to change/hide post meta for post slider element
* Added option to change/hide bookmark button
* Updated options description for better understanding
* Improved folder and file structure
* Improved Visual Composer compatibility
* Fixed search page layout issues
* Fixed mix layout responsive issues
* Fixed header position issue
* Various other improvements

Version 1.0.3
* Added new homepage - home 6
* Added option to hide Instagram feed on specific pages
* Improved theme setting options

Version 1.0.2
* Updated theme description
* Improved Visual Composer compatibility
* Improved SEO

Version 1.0.1
* Added full Visual Composer support
* Updated demo content
* Fixed a few bugs

Version 1.0.0
* Initial release
