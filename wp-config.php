<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'gillion');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'admin123');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'T*1!y?T-v;aBMWE-6{i8cTdaC:?j|>*k=!zdsKgIgk;D:_!O}@9?XEklU;?&mre@');
define('SECURE_AUTH_KEY',  'TUM_s$I`|v> R*m)S6BK|L+^b]ikwT}fk>[]s6axs[*j[jX`,22y`fBt,7g;(^4e');
define('LOGGED_IN_KEY',    'qiu]eBTxm^Zgt2xbE:/ZD6cU*ELrmRz@zs:oJMsqg:$9 z%M^Xs[G?5gM41+o}.X');
define('NONCE_KEY',        '.PJb4o74kvo&_p>xi{9:EJq7n)`mdds3JtK9|:u4qB,^@nQjQNQpo8&C]F(5Tg&:');
define('AUTH_SALT',        'ed%EOVTZob*wH9VZ(*4k9VM6#k)5;;kzzQ6G~,eYTi~SX_}TrZh,w|^~J7Qt[iJ=');
define('SECURE_AUTH_SALT', ';6Sk0og8cE.FDPwUvO5xd<>0$@,82KJ9WZEn>J!tY[R~=4(8Tg.6zdE82x0P{zmD');
define('LOGGED_IN_SALT',   '|K@kv)t^)m8*=KXT%VVQ/f HM4]_=6_,HgV(Qz+BGJ{]=hY([Y_)FfZG{tupkaMX');
define('NONCE_SALT',       'vAb,/~%P  p7k0og61qZ))&A(u@pK5iHu;#i,Fv|p>OJ[6Pcgz0dj<|:+cTHYE6q');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
